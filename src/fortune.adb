--
-- Copyright (c) 2017 Tero Koskinen <tero.koskinen@iki.fi>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--
with Ada.Strings.Unbounded;
with Ada.Command_Line;
with Quotes;
with Hauki.Containers;
with Ada.Text_IO;
with Ada.Numerics.Discrete_Random;

procedure Fortune is
   use Ada.Strings.Unbounded;
   use Ada.Command_Line;
   use type Hauki.Containers.Count_Type;
   use type Quotes.String_List.Cursor;

   package Random_Quote is
     new Ada.Numerics.Discrete_Random (Hauki.Containers.Count_Type);
   use Random_Quote;

   Files : Quotes.String_List.List;
   All_Quotes : Quotes.String_List.List;
   Pos : Quotes.String_List.Cursor;
   Quote_List_Len : Hauki.Containers.Count_Type;
   
   G : Generator;
   Quote_No : Hauki.Containers.Count_Type;
begin
   Reset (G);
   for I in 1 .. Argument_Count loop
      Quotes.String_List.Append (Files, To_Unbounded_String (Argument (I)));
   end loop;
   
   Quotes.Read_Quotes (Files, All_Quotes);
   
   if Quotes.String_List.Length (All_Quotes) = 0 then
      Ada.Text_IO.Put_Line ("No quotes!");
      return;
   end if;
   Quote_List_Len := Quotes.String_List.Length (All_Quotes);
   
   Quote_No :=
     Random(G) / (Hauki.Containers.Count_Type'Last / Quote_List_Len) + 1;
   if Quote_No > Quote_List_Len then
      Quote_No := Quote_List_Len;
   end if;
   
   Pos := Quotes.String_List.First (All_Quotes);
   for I in 2 .. Quote_No loop
      Pos := Quotes.String_List.Next (Pos);
   end loop;
   Ada.Text_IO.Put (To_String (Quotes.String_List.Element (Pos)));
end Fortune;
