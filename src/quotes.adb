--
-- Copyright (c) 2017 Tero Koskinen <tero.koskinen@iki.fi>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--
with Ada.Text_IO;

package body Quotes is
   procedure Read_Quotes_From_File (Filename : String;
                                    Quotes_Data : in out String_List.List)
   is
      Handle : Ada.Text_IO.File_Type;
      Line_Char_Count : Natural;
      Line : String (1..400);
      Complete_Line : Unbounded_String;
   begin
      Ada.Text_IO.Open (Handle, Ada.Text_IO.In_File, Filename);
      loop
         exit when Ada.Text_IO.End_Of_File (Handle);
         Ada.Text_IO.Get_Line
           (File => Handle, Item => Line, Last => Line_Char_Count);
         if Line_Char_Count > 0 then
            if Line (1.. Line_Char_Count) = "%" then
               String_List.Append (Quotes_Data, Complete_Line);
               Complete_Line := Null_Unbounded_String;
            else
               Append (Complete_Line, Line (1..Line_Char_Count));
               Append (Complete_Line, Character'Val(13) & Character'Val(10));
            end if;
         end if;
      end loop;
      Ada.Text_IO.Close (Handle);
   end Read_Quotes_From_File;

   procedure Read_Quotes
     (Filenames   : in     String_List.List;
      Quotes_Data : in out String_List.List) is
      
      use type String_List.Cursor;
      
      Filename_Pos : String_List.Cursor;
   begin
      Filename_Pos := String_List.First (Filenames);
      
      loop
         exit when Filename_Pos = String_List.No_Element;
            
         Read_Quotes_From_File
            (To_STring (String_List.Element (Filename_Pos)), Quotes_Data);
         Filename_Pos := String_List.Next (Filename_Pos);
      end loop;
   end Read_Quotes;
end Quotes;
