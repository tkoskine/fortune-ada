# Fortune application in Ada

This repository contains a simple "fortune" application written in Ada.

Usage:

    fortune <fortunes1.txt> [fortunes2.txt] .. [fortunesN.txt]


Fortune files are plain text files where each fortune text
is separated by '%' character on its own line.

Example:

    My first fortune text.
    %
    My Second fortune text.
    Another line.
    %
    My third and last fortune. Notice % at the end.
    %

License:

The source code is distributed under ISC license. See LICENSE.txt or
source file headers for the details.

Author:

Tero Koskinen <tero.koskinen@iki.fi>

